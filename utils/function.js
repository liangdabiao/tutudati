import {	templateSubscribe } from "@/utils/api/message.js"

export default {
	showToast(title, time = 3000, mask = false) {
		uni.showToast({
			title: title,
			duration: time,
			icon: "none",
			mask: mask,
		})
	},
	navigatorTo(url) {
		uni.navigateTo({
			url: url,
			fail(res) {
				console.log(res, url)
				uni.showToast({
					title: "地址不存在",
					icon: "none",
					mask: true,
				})
			}
		})
	},
	previewQRCodeImage() {
	  wx.previewImage({
	    urls: ['http://qiniucloudtest.qqdeveloper.com/9a24e2f9cad6b7c328bb7f4af49fe1b0.png']
	  })
	},
	tnHome() {
		uni.navigateTo({
			url: "/subpages/home/home/home",
			fail(res) {
				uni.showToast({
					title: res.errMsg,
					duration: 5000,
					icon: 'none'
				})
			}
		})
	},
	tnRelunch(url) {
		uni.reLaunch({
			url: url
		})
	},
	templateSubscribe(template) {
		return new Promise((resolve, reject) => {
			uni.requestSubscribeMessage({
				tmplIds: [template],
				success(res) {
					console.log(res)
					if (res.errMsg == 'requestSubscribeMessage:ok' && res[template] == 'accept') {
						templateSubscribe({
							template_id: template
						}).then(res1 => {
							let msg = "订阅失败"
							if (res1.code == 100) {
								msg = "订阅成功"
							}
							uni.showToast({
								title: msg,
								icon: "none",
							})
						}).catch((error) => {
							uni.showToast({
								title: error,
								icon: 'none',
								duration: 3000
							})
						})
					} else if (res.errMsg == 'requestSubscribeMessage:ok' && res[template] == 'reject') {
						uni.showModal({
							title: '订阅提示',
							content: '你已拒绝接收该消息，请打开右上角，设置按钮，在订阅消息中开启接收该消息，接收订阅有助你于实时接收试题最新更新状态。',
							success: function (res) {
								if (res.confirm) {
									console.log('用户点击确定');
								} else if (res.cancel) {
									console.log('用户点击取消');
								}
							}
						});
					} else {
						uni.showToast({
							title: res.errMsg,
							icon: 'none',
							duration: 3000
						})
					}
				},
				fail(res) {
					console.log("订阅失败", res)
					uni.showToast({
						title: res.errMsg,
						icon: 'none',
						duration: 3000
					})
				}
			})
		})
	},
}