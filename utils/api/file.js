import request from "@/utils/request"

/**
 * 上传单个图片资源
 * @param {Object} fileUrl
 */
export function uploadImage(fileUrl) {
	return request.upload('upload/image', {
		fileType: "image",
		name: "image",
		filePath: fileUrl,
	}).then(res => {
		return res
	})
}


/**
 * 获取文件资源全量分类
 */
export function getResourceCategoryList() {
	return request.get("resource/category/list").then(res => {
		return res
	})
}

/**
 * 获取文件资源列表
 * @param {Object} params
 */
export function getResourceList(params) {
	return request.get("resource/getList", {
		params: params
	}).then(res => {
		return res
	})
}

/**
 * 获取文件资源内容
 * @param {Object} params
 */
export function getResourceContent(params) {
	return request.get("resource/getContent", {
		params: params
	}).then(res => {
		return res
	})
}

/**
 * 获取资源下载链接
 * @param {Object} params
 */
export function getDownloadUrl(params) {
	return request.get("resource/getDownloadUrl", {
		params: params
	}).then(res => {
		return res
	})
}