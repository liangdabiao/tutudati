import request from "@/utils/request"

/**
 * 用户微信小程序登录
 * @param {Object} params
 */
export function userLogin(params) {
	return request.post("wechatMiniCodeLogin", params).then(res => {
		return res
	})
}

/**
 * 获取用户信息
 */
export function getAvatarUserInfo() {
	return request.get("user/avatar/info", {}).then(res => {
		return res
	})
}

/**
 * 获取用户基础信息
 */
export function getUserBasicInfo() {
	return request.get("auth/getUserBasicInfo", {}).then(res => {
		return res
	})
}

/**
 * 获取用户基础信息(非强制鉴权)
 */
export function getUserBasicInfoNoAuth() {
	return request.get("noauth/getUserBasicInfo", {}).then(res => {
		return res
	})
}

/**
 * 更新用户基础信息
 * @param {Object} params
 */
export function updateUserBasicInfo(params) {
	return request.post("auth/updateUserBasicInfo", params).then(res => {
		return res
	})
}

/**
 * 检查用户是否处于登录状态
 */
export function checkUserLoginState() {
	return request.options("user/check/login").then(res => {
		return res
	})
}

/**
 * 会员配置列表
 */
export function getMemberConfigList() {
	return request.get("user/member/config/getList").then(res => {
		return res
	})
}

/**
 * 会员权益详情
 */
export function getMemberConfigContent(params) {
	return request.get("user/member/config/getContent", {
		params: params
	}).then(res => {
		return res
	})
}