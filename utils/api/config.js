import request from "@/utils/request"

/**
 * 获取首页导航菜单
 * @param {Object} params
 */
export function getHomeMenuList(params) {
	return request.get("getMenuList", {
		params: params,
	}).then(res => {
		return res
	})
}

/**
 * 获取首页顶部banner
 * @param {Object} params
 */
export function getBannerList(params) {
	return request.get("getBannerList", {
		params: params,
	}).then(res => {
		return res
	})
}

/**
 * 获取用户专业配置列表
 */
export function getUserProfessionList() {
	return request.get("config/profession/getList", {}).then(res => {
		return res
	})
}

/**
 * 获取首页公告列表
 * @param {Object} params
 */
export function getNoticeList(params) {
	return request.get("getNoticeList", {
		params: params,
	}).then(res => {
		return res
	})
}